<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0">

	<xsl:output indent="yes" />

	<xsl:param name="subtitle" select="subtitle" />

	<xsl:template match="json">
		<rss version="2.0">
			<channel>
				<title>
					<xsl:text>Sbazar.cz | </xsl:text>
					<xsl:value-of select="$subtitle" />
				</title>
				<language>cs</language>
				<xsl:apply-templates select="json-to-xml(.)/map/array[@key='results']"
									 xpath-default-namespace="http://www.w3.org/2005/xpath-functions"/>
			</channel>
		</rss>
	</xsl:template>

	<xsl:template match="map" xpath-default-namespace="http://www.w3.org/2005/xpath-functions">
		<item>
			<title>
				<xsl:value-of select="string[@key='name']" />
				<xsl:text> - </xsl:text>
				<xsl:value-of select="number[@key='price']" />
				<xsl:text> Kč</xsl:text>
			</title>
			<link>
				<xsl:text>https://www.sbazar.cz/</xsl:text>
				<xsl:value-of select="map[@key='user']/map[@key='user_service']/string[@key='shop_url']" />
				<xsl:text>/detail/</xsl:text>
				<xsl:value-of select="string[@key='seo_name']" />
			</link>
			<description>
				<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
				<img>
					<xsl:attribute name="src">
						<xsl:text>https:</xsl:text>
						<xsl:value-of select="array[@key='images']/map/string[@key='url']" />
						<xsl:text>?fl=exf|res,280,280,3|jpg,80,,1</xsl:text>
					</xsl:attribute>
				</img>
			<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
			</description>
			<pubDate>
				<xsl:call-template name="dateTime-to-RFC-2822">
					<xsl:with-param name="dateTime" select="string[@key='edit_date']" />
				</xsl:call-template>
			</pubDate>
			<guid isPermaLink="false">
				<xsl:value-of select="number[@key='id']" />
			</guid>
		</item>
	</xsl:template>

	<xsl:template name="dateTime-to-RFC-2822">
		<xsl:param name="dateTime"/>
		<!-- extract components -->
		<xsl:variable name="year" select="number(substring($dateTime, 1, 4))" />
		<xsl:variable name="month" select="number(substring($dateTime, 6, 2))" />
		<xsl:variable name="day" select="number(substring($dateTime, 9, 2))" />
		<!-- calculate day-of-week using Zeller's_congruence -->
		<xsl:variable name="a" select="number($month &lt; 3)"/>
		<xsl:variable name="m" select="$month + 12*$a"/>
		<xsl:variable name="y" select="$year - $a"/>
		<xsl:variable name="K" select="$y mod 100"/>
		<xsl:variable name="J" select="floor($y div 100)"/>
		<xsl:variable name="h" select="($day + floor(13*($m + 1) div 5) + $K + floor($K div 4) + floor($J div 4) + 5*$J + 6) mod 7"/>
		<!-- construct output -->
		<xsl:value-of select="substring('SunMonTueWedThuFriSat', 3 * $h + 1, 3)"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="$day"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="substring('JanFebMarAprMayJunJulAugSepOctNovDec', 3 * ($month - 1) + 1, 3)"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$year"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="substring-after($dateTime, 'T')" />
		<xsl:text> GMT</xsl:text>
	</xsl:template>

</xsl:stylesheet>
