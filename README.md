# Sbazar RSS

Converts Sbazar API JSON results to RSS feed.

SaxonJS requires stylesheet to be in SEF format.
To convert XML stylesheet to SEF format, run:
```shell
./compile-stylesheet.sh
```
