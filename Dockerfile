FROM node:18-alpine AS npm-prod

USER 1000

WORKDIR /app

COPY --chown=1000:1000 \
    package.json \
    package-lock.json \
    ./

RUN npm install --omit=dev

FROM npm-prod AS compiled

RUN npm install

COPY \
    index.ts \
    tsconfig.json ./

RUN ./node_modules/.bin/tsc

FROM node:18-alpine

USER 1000

WORKDIR /app

COPY --from=compiled /app/index.js .
COPY --chown=1000:1000 --from=npm-prod /app/node_modules/ node_modules/
COPY --chown=1000:1000 \
    package.json \
    stylesheet.sef.json \
    favicon.html \
    ./

EXPOSE 80
