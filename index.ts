import SaxonJS from 'saxon-js'
import {promises as fs} from "fs"
import * as https from "https";
import * as http from "http"
import {IncomingMessage} from "http"
import * as path from "path"

async function get(url: URL): Promise<Buffer> {
	return new Promise((resolve) => {
		https.get(url.href, res => {
			let chunks = []
			res.on('data', chunk => chunks.push(chunk))
			res.on('end', () => resolve(Buffer.concat(chunks)))
		})
	})
}

function getQuery(request: IncomingMessage): string | null {
	const url = new URL(request.url, `https://${request.headers.host}`)
	return url.searchParams.get('q')
}

function getApiURL(phrase: string): URL {
	const apiURL = new URL('https://www.sbazar.cz/api/v1/items/search')
	apiURL.searchParams.append('phrase', phrase)
	apiURL.searchParams.append('limit', '50')
	return apiURL
}

const server = http.createServer(async (request, response) => {
	try {
		const query = getQuery(request)
		if (query === null) {
			response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'})
			response.end(await fs.readFile('favicon.html'))
			return
		}

		const json = await get(getApiURL(query))
		const xml = `<?xml version="1.0" encoding="utf-8"?>\n<json>${json}</json>`
		const sourcePath = path.join(process.cwd(), 'source.xml')
		await fs.writeFile(sourcePath, xml)

		const result = await SaxonJS.transform({
			stylesheetFileName: "stylesheet.sef.json",
			sourceFileName: sourcePath,
			destination: "serialized",
			stylesheetParams: {subtitle: query}
		}, "async")

		response.writeHead(200, {
			'Content-Type': 'text/xml; charset=utf-8',
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			'Pragma': 'no-cache',
			'Expires': '0',
		})
		response.end(result.principalResult)
	} catch (error) {
		response.writeHead(500);
		response.end(error.toString());
	}
})

server.listen(80);
